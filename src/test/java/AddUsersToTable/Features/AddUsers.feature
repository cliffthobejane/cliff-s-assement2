@first
Feature:Add user to the table successfully

Background:
  Given I navigate to web_tables
  And I am located on adding users screen
  When I select add users button

 Scenario:Add user number one
   And I enter user details for user one
     | FName1 | LName1 | User1| Pass1  | Company AAA | Admin | admin@mail.com  | 082555|
   Then Ensure username one is unique
     | FName1 | LName1 | User1| Pass1  | Company AAA | Admin | admin@mail.com  | 082555|
   And Ensure user number one details are added to the table
     | FName1 | LName1 | User1| Pass1  | Company AAA | Admin | admin@mail.com  | 082555|



  Scenario:Add user number two
    And I enter user details for user two
      | FName2 | LName2 | User2| Pass2  | Company BBB | Customer | customer@mail.com  | 082444|
    Then Ensure username two is unique
      | FName2 | LName2 | User2| Pass2  | Company BBB | Customer | customer@mail.com  | 082444|
    And Ensure user number two details are added to the table
      | FName2 | LName2 | User2| Pass2  | Company BBB | Customer | customer@mail.com  | 082444|




