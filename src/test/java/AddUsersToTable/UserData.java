package AddUsersToTable;

public class UserData {
    private String first_name;
    private String last_name;
    private String username;
    private String password;
    private String customer;
    private String role;
    private String email;
    private String cell;

    public UserData(String first_name,String last_name,String username,String password,String customer,String role,String email,String cell){
        this.first_name=first_name;
        this.last_name=last_name;
        this.username=username;
        this.password=password;
        this.customer=customer;
        this.role=role;
        this.email=email;
        this.cell=cell;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCustomer() {
        return customer;
    }

    public String getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }

    public String getCell() {
        return cell;
    }
}
