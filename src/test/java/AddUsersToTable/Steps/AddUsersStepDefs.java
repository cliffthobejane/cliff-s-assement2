package AddUsersToTable.Steps;

import AddUsersToTable.Helper.AddUsersHelper;
import AddUsersToTable.UserData;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

import java.util.List;

public class AddUsersStepDefs {
    AddUsersHelper addUsersHelper =new AddUsersHelper();
//    public AddUsersStepDefs(){
//        this.addUsersHelper=new AddUsersHelper();
//    }

    @Before(value="@first")
    public void setUp(){
        addUsersHelper.setUp();

    }
    @Given("^I navigate to web_tables$")
    public void i_navigate_to_web_tables() throws Throwable {
       addUsersHelper.navigate_to_user_main_page();

    }

    @Given("^I am located on adding users screen$")
    public void i_am_located_on_adding_users_screen() throws Throwable {
        addUsersHelper.verifyYouOnAddUsersPage();

    }

    @When("^I select add users button$")
    public void i_select_add_users_button() throws Throwable {
       addUsersHelper.clickAddUser();
    }

    @When("^I enter user details for user one$")
    public void i_enter_user_details_for_user_one(DataTable data) throws Throwable {
        List<List<String>> userDataList=data.raw();

      addUsersHelper.inputUserData(new UserData(userDataList.get(0).get(0),userDataList.get(0).get(1),userDataList.get(0).get(2),userDataList.get(0).get(3),userDataList.get(0).get(4),userDataList.get(0).get(5),userDataList.get(0).get(6),userDataList.get(0).get(7)));
      Thread.sleep(1000);


    }

    @Then("^Ensure username one is unique$")
    public void ensure_username_one_is_unique(DataTable users) throws Throwable {
        List<List<String>> userDataList=users.raw();

       Assert.assertTrue(addUsersHelper.ensureUsernameAreUnique(new UserData(userDataList.get(0).get(0),userDataList.get(0).get(1),userDataList.get(0).get(2),userDataList.get(0).get(3),userDataList.get(0).get(4),userDataList.get(0).get(5),userDataList.get(0).get(6),userDataList.get(0).get(7))));



    }

    @Then("^Ensure user number one details are added to the table$")
    public void ensure_user_number_one_details_are_added_to_the_table(DataTable data) throws Throwable {
        List<List<String>> userDataList=data.raw();

        Assert.assertTrue(addUsersHelper.ensureUsernameAreUnique(new UserData(userDataList.get(0).get(0),userDataList.get(0).get(1),userDataList.get(0).get(2),userDataList.get(0).get(3),userDataList.get(0).get(4),userDataList.get(0).get(5),userDataList.get(0).get(6),userDataList.get(0).get(7))));

    }

    @When("^I enter user details for user two$")
    public void i_enter_user_details(DataTable data) throws Throwable {
        List<List<String>> userDataList=data.raw();

        addUsersHelper.inputUserData(new UserData(userDataList.get(0).get(0),userDataList.get(0).get(1),userDataList.get(0).get(2),userDataList.get(0).get(3),userDataList.get(0).get(4),userDataList.get(0).get(5),userDataList.get(0).get(6),userDataList.get(0).get(7)));


    }

    @Then("^Ensure username two is unique$")
    public void ensure_username_two_is_unique(DataTable users) throws Throwable {
        List<List<String>> userDataList=users.raw();

        Assert.assertTrue(addUsersHelper.ensureUsernameAreUnique(new UserData(userDataList.get(0).get(0),userDataList.get(0).get(1),userDataList.get(0).get(2),userDataList.get(0).get(3),userDataList.get(0).get(4),userDataList.get(0).get(5),userDataList.get(0).get(6),userDataList.get(0).get(7))));


    }

    @Then("^Ensure user number two details are added to the table$")
    public void ensure_user_number_two_details_are_added_to_the_table(DataTable data) throws Throwable {
        List<List<String>> userDataList=data.raw();

        Assert.assertTrue(addUsersHelper.ensureUsernameAreUnique(new UserData(userDataList.get(0).get(0),userDataList.get(0).get(1),userDataList.get(0).get(2),userDataList.get(0).get(3),userDataList.get(0).get(4),userDataList.get(0).get(5),userDataList.get(0).get(6),userDataList.get(0).get(7))));


    }
    @After(value="@first")
    public void closeDriversForAllScenarios(){
        addUsersHelper.closeDrivers();
    }

}
