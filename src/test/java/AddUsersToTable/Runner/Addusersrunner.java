package AddUsersToTable.Runner;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/java/AddUsersToTable/Features/AddUsers.feature"},
        glue = {"AddUsersToTable.Steps"},
        monochrome = true,
        tags ={}
        )

public class Addusersrunner extends AbstractTestNGCucumberTests{
}
