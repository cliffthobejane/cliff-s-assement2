package AddUsersToTable.Helper;

import AddUsersToTable.UserData;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddUsersHelper {
    WebDriver driver;
//    private Page page;
//
//    public AddUsersHelper(){
//       page=new Page(driver.switchTo().defaultContent()) ;
//    }

    public void setUp(){
       System.setProperty("webdriver.chrome.driver","src/test/java/AddUsersToTable/drivers/chromedriver.exe");
       this.driver=new ChromeDriver();
       this.driver.manage().window().maximize();
       this.driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
       this.driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);

    }
    public void navigate_to_user_main_page(){
        driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
        driver.get("http://www.way2automation.com/angularjs-protractor/webtables/");
        driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
    }
    public void verifyYouOnAddUsersPage(){
       WebElement addUser =driver.findElement(By.xpath("*//button[contains(text(),'Add User')]"));
       driver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
        Assert.assertNotNull(addUser);

    }

    public void clickAddUser(){
        driver.findElement(By.xpath("*//button[contains(text(),'Add User')]")).click();

    }
    public void inputUserData(UserData data){

        WebElement first_name=driver.findElement(By.xpath("//input[@name='FirstName']"));
        first_name.sendKeys(data.getFirst_name());
        WebElement last_name=driver.findElement(By.xpath("//input[@name='LastName']"));
        last_name.sendKeys(data.getLast_name());
        WebElement username=driver.findElement(By.xpath("//input[@name='UserName']"));
        username.sendKeys(data.getUsername());
        WebElement password=driver.findElement(By.xpath("//input[@name='Password']"));
        password.sendKeys(data.getPassword());

        if(data.getCustomer().equalsIgnoreCase("Company AAA")){
            driver.findElement(By.xpath("//input[@value='15']")).click();
        }
        else
            if(data.getCustomer().equalsIgnoreCase("")){
                driver.findElement(By.xpath("//input[@value='16']")).click();
            }


        WebElement selectRolePopUp=driver.findElement(By.xpath("//select[@name='RoleId']"));
        selectRolePopUp.click();
        WebElement selectRole=driver.findElement(By.xpath("//option[contains(text(),'"+data.getRole()+"')]"));
        selectRole.click();
        WebElement email=driver.findElement(By.xpath("//input[@name='Email']"));
        email.sendKeys(data.getEmail());
        WebElement cell=driver.findElement(By.xpath("//input[@name='Mobilephone']"));
        cell.sendKeys(data.getCell());

        driver.findElement(By.xpath("//button[@class='btn btn-success']")).click();

    }
    public Boolean ensureUsernameAreUnique(UserData data) {

        List<WebElement> webElements = driver.findElements(By.xpath("/html/body/table/tbody/tr"));
        int numberOfRows = webElements.size();
        int initialiteration=0;
        int samenumberofusername=0;

        int column=1;

        while (initialiteration<numberOfRows){

            if(driver.findElement(By.xpath("/html/body/table/tbody/tr["+column+"]")).getText().equalsIgnoreCase(data.getUsername())){
                samenumberofusername++;
            }
            initialiteration++;
            if(samenumberofusername>=2){
                return false;
            }
        }


        return true;
    }

    public Boolean usersareaddedtoPage(UserData data){
        List<WebElement> webElements = driver.findElements(By.xpath("/html/body/table/tbody/tr"));
        int numberOfRows = webElements.size();
        int initialiteration=0;
        int samenumberofusername=0;

        int column=1;

        while (initialiteration<numberOfRows){


            if(driver.findElement(By.xpath("/html/body/table/tbody/tr["+column+"]")).getText().equalsIgnoreCase(data.getUsername())){
                samenumberofusername++;
            }
            initialiteration++;
            if(samenumberofusername>=2){
                return false;
            }
        }


        return true;



    }




    public void closeDrivers(){
        driver.close();
        driver.quit();
    }
}
